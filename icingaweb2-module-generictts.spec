# GenericTTS Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name generictts

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        2.0.0
Release:        %{revision}%{?dist}
Summary:        GenericTTS - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

%description
Generic TTS implements Icinga Web 2's ticket hook for replacing ticket
patterns with links to your trouble ticket system (TTS). Icinga Web 2's core
module monitoring for example uses the ticket hook for acknowledgements,
downtimes and comments. Other modules may use the ticket hook for all kinds
of text too.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

%clean
rm -rf %{buildroot}

%preun
set -e

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%files
%doc README.md COPYING

%defattr(-,root,root)
%{basedir}

%changelog
* Mon Sep 09 2019 Markus Frosch <markus.frosch@icinga.com> - 2.0.0-1
- Release version 2.0.0-1

* Wed Aug 28 2019 Markus Frosch <markus.frosch@icinga.com> - 2.0.0-1
- Initial package version
